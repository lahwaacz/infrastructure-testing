# Arch infrastructure testing

The motivation for this "project" is running [Ansible](https://www.ansible.com/)
tasks in a local container for testing the
[Arch Linux infrastructure](https://gitlab.archlinux.org/archlinux/infrastructure/)
configuration.

The workflow starts with a base Arch container providing a clean system with
preconfigured SSH access. After starting the container, the system can be
managed by Ansible. The container itself is managed by the user. For example,
depending on your setup, you can create snapshots, add data volumes for testing
services, etc.

Below are hints for setting up a few container types. If you have a different
favourite setup, feel free to share!

## Set up a container

- [Docker](https://www.docker.com/) – see [docker/README.md](./docker/README.md)
- [systemd-nspawn](https://wiki.archlinux.org/index.php/systemd-nspawn) – see
  [systemd-nspawn/README.md](./systemd-nspawn/README.md)

## Ansible

The Arch infrastructure repository is cloned as a git submodule in
`./arch_infrastructure/`. Hence, make sure to initialize git submodules after
cloning the repo:

    git submodule update --init --recursive

The playbooks, variables etc. for managing local containers are in
`./local_infrastructure/` (maintaining a separate directory is needed to get
around the vaulted variables in the main repository).

The Ansible roles, plugins and library scripts are taken directly from the main
repository (see `ansible.cfg` in `local_infrastructure`). However, the roles do
not work out of the box inside local containers (yet). Also some roles do not
make sense at all for local containers (e.g. `archusers`, `root_ssh`,
`borg-client` etc.) In general we also cannot use Let's Encrypt in containers so
some services have to be modified to be accessible over plain HTTP.

> __Note:__
> The `arch_infrastructure` submodule tracks a custom repository/branch with
> the necessary changes by default:
> https://github.com/lahwaacz/archlinux-infrastructure/commits/local-containers-wip

### Getting started

- configure your container in `local_infrastructure/hosts`
- create a playbook for your container in `local_infrastructure/playbooks/`
- configure variables for the roles used in your playbooks – see `group_vars`
  and `host_vars` in `local_infrastructure` and, perhaps more importantly,
  `arch_infrastructure`
- run the playbook:

      cd local_infrastructure
      ansible-playbook playbooks/<name>.yml

### SSL certificates for local containers

The Arch infrastructure uses the `certbot` role to automatically retrieve SSL
certificates from [Let's Encrypt](https://letsencrypt.org/). However, this
cannot work for _localhost_ or local containers. See [Certificates for
localhost](https://letsencrypt.org/docs/certificates-for-localhost/) for more
information.

To provide HTTPS for local containers, we can use the tool [mkcert](
https://github.com/FiloSottile/mkcert) to create and install a local CA in the
system trust store, and to generate locally-trusted certificates which can be
installed by Ansible in the local containers. You can just replace the `certbot`
role with the `mkcert` role in your playbook and enjoy HTTPS in local
containers.

Before you run the playbook with the `mkcert` role, you need to manually install
a local CA. Make sure that the `mkcert` package is installed on the host and run

    mkcert -install

Note that `mkcert` will [use sudo to change the system trust store](
https://github.com/FiloSottile/mkcert/issues/236).
