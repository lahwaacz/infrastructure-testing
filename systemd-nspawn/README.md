## Prerequisites

[systemd-nspawn](https://wiki.archlinux.org/index.php/systemd-nspawn) works on
any Arch host system. Configuring _systemd-networkd_ and _systemd-resolved_ on
the host may simplify networking setup (see below).

## Create a container

First copy your public SSH key to `./files/authorized_keys`.

Run the `create-container` script (uses `sudo`):

    ./create-container <container-name>

This creates a container in `/var/lib/machines/<container_name>` which can be
managed with [machinectl](
https://wiki.archlinux.org/index.php/systemd-nspawn#machinectl).

## Tips and tricks

- The name of the container implies its hostname (see the `--machine` and
  `--hostname` flags).
- Hostname resolution for local containers with private network (see below)
  managed by `machinectl` works out-of-the-box with `nss-mymachines(8)`, so
  you can access the container with `ssh root@<container>`.
- Containers can be configured in `/etc/systemd/nspawn/<container>.nspawn`.
- Note there is an `Ephemeral=yes` option to run the container from a temporary
  snapshot.
- Set `Timezone=off` to let the management of `/etc/localtime` up to the
  container.
- See [systemd-nspawn#Networking](https://wiki.archlinux.org/index.php/Systemd-nspawn#Networking)
  on ArchWiki for networking details.
